import colorsys
from time import sleep
import serial
import multiprocessing as mp
from flask import Flask, jsonify, request



NUM_LEDS = 90
LEDS_PER_TILE = 8

class Task: 
    def __init__(self, command, tile_id, r,g,b): 
        self.command = int(command)
        self.tile_id = int(tile_id)
        self.r = r
        self.g = g
        self.b = b
        

        

arduino = serial.Serial('/dev/cu.usbmodem14701', 9600, timeout=.1)

    
# Commands

def fill_color_by_id(id,r,g,b):
    current_tile_start = LEDS_PER_TILE * id
    for i in range(0, LEDS_PER_TILE):
        arduino.write(
            f'<{ current_tile_start + i },{r},{g},{b}>'.encode())
    arduino.write(
        f'<-1>'.encode())

# MP


myq = mp.Queue()

def q_get_all(queue):

    result_list = []
    while not queue.empty():
        result_list.append(queue.get())

    return result_list


def painter(queue):
    
    animated = dict()
    
    while True:
        
        
        
        # Process HAAS requests
        
        todo = q_get_all(queue=queue)
        
        for task in todo:
            try:
                animated.pop(task.tile_id)
            except:
                pass 
            
            match int(task.command):
                case 1:
                    fill_color_by_id(task.tile_id, task.r, task.g, task.b)
                case 3:
                    # convert to hsv and get target
                    start = list(colorsys.rgb_to_hsv(task.r/255, task.g/255, task.b/255))
                    target = start.copy()
                    
                    if start[0] + 0.2 > 1:
                        target[0] = target[0] - 0.05
                    else:
                        target[0] = target[0] + 0.05
                    
                    # convert back to rgb
                        
                    start = list(colorsys.hsv_to_rgb(start[0], start[1], start[2]))
                    target = list(colorsys.hsv_to_rgb(
                        target[0], target[1], target[2]))
                    
                    # convert from 0.0 - 1.0 to 0 - 255
                    for i in range(3):
                        start[i] = int(start[i] * 255)
                        target[i] = int(target[i] * 255)
                    
                    
                    animated.update(
                        {task.tile_id: {'task': task, 'target': target, 'start': start, 'progress': 0, 'reverse': False}})  # [r,g,b]
                case _:
                    print("unknown command")
                    
        # Update animations
        
        #1 get 1 percent of animation: step[r,g,b]
        #2 if difference current/target on any channel is less than step, set current to target, flip target and start
        #3 if not, add step to current 
        
        for anim in list(animated.values()):
            
            if anim['reverse']:
                anim['progress'] = anim['progress'] - 0.05
            else:
                anim['progress'] = anim['progress'] + 0.05
            
            r = int((1 - anim['progress']) *
                    anim['start'][0] + anim['progress'] * anim['target'][0])
            g = int((1 - anim['progress']) *
                    anim['start'][1] + anim['progress'] * anim['target'][1])
            b = int((1 - anim['progress']) *
                    anim['start'][2] + anim['progress'] * anim['target'][2])
            
            if anim['progress'] >= 1 or anim['progress'] <= 0:
                anim['reverse'] = not anim['reverse']
                
                
            fill_color_by_id(id= anim['task'].tile_id, r= r, g= g, b= b)
                
                
            
        sleep(0.05)
            






# HAAS reciever


app = Flask(__name__)

state = [False, False, False]


@app.route("/light_api")
def status():
    global state
    return jsonify({'on': state})


@app.route("/light_api", methods=['POST'])
def toggle():
    #print(request.json)
    global state
    body = request.get_json(force=True)
    print(body)
    try:
        if body['ignore']:
            print('ignored')
            return jsonify({'on': state})
    except:
        pass
    
    hue = 0.0
    sat = 0.0
    brightness = 255
    if 'hue' in body:
        hue = body['hue'] / 360.0
    if 'sat' in body:
        sat = body['sat'] / 100.0
    if 'brightness' in body:
        brightness = body["brightness"] / 255.0
    rgb = colorsys.hsv_to_rgb(hue, sat, brightness)
    rgb_r = int(rgb[0] * 255)
    rgb_g = int(rgb[1] * 255)
    rgb_b = int(rgb[2] * 255)
    

    
    tile_id = int(body['id']) 
    #tile_id = 0
    #is_on_template: "{{ value_json.on }}"
    
    if body['on']:
        
        
        myq.put(Task(float(body['command']),float(tile_id), rgb_r, rgb_g, rgb_b))

                                            
        state[int(tile_id)] = True
    else:
        fill_color_by_id(tile_id, 0, 0, 0)
        state[int(tile_id)] = False

    return jsonify({'on': state})









if __name__ == '__main__':
    
    p = mp.Process(target=painter, args=(myq,))
    p.start()
    
    app.run(host="0.0.0.0", debug=True, use_debugger=False, use_reloader=False)
    

# Random code snippets

"""
if body['on']:
        print(
            f'<hsv by id <1,0,{hue},{sat},{brightness}>'.encode())
        arduino.write(
            f'<1,0,{interp(hue,[0,359],[0,255])},{sat},{brightness}>'.encode())
        state = True
    else:
        print(
            f'<hsv by id <1,0,0,0,0>'.encode())
        arduino.write(
            f'<1,0,0,0,0>'.encode())
        state = False
        
        

        
        
        
"""